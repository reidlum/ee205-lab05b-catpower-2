///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file megaton.h
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date 10_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once
const double MEGATONS_IN_A_JOULE = 2.3900573614e-16;
const char MEGATON       = 'm';

extern double fromMegatonToJoule( double megaton );
extern double fromJouleToMegaton( double joule );

