///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.cpp
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date 10_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include "cat.h"
double fromCatPowerToJoule( ) {
   return 0.0;  // Cats do no work
}

