///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date 10_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once
const double GGES_IN_A_JOULE = 8.2440230833e-9;
const char GGE           = 'g';

extern double fromGgeToJoule( double gge );
extern double fromJouleToGges( double joule );

