///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date 10_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include "gge.h"
double fromGgeToJoule( double gge ) {
   return gge / GGES_IN_A_JOULE;
}

double fromJouleToGges( double joule ) {
   return joule * GGES_IN_A_JOULE;
}

